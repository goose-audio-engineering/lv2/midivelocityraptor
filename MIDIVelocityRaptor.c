/*
  LV2 MIDI Compressor plugin
  Copyright 2020 Walter Goossens <waltergoossens@creative-embedded.com>

  Based on:

  LV2 Fifths Example Plugin
  Copyright 2014-2016 David Robillard <d@drobilla.net>

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef __cplusplus
#    include <stdbool.h>
#endif

#include "lv2/lv2plug.in/ns/ext/atom/util.h"
#include "lv2/lv2plug.in/ns/ext/log/logger.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/ext/patch/patch.h"
#include "lv2/lv2plug.in/ns/ext/state/state.h"
#include "lv2/lv2plug.in/ns/ext/urid/urid.h"
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"
#include "lv2_util.h"
#include "./uris.h"

enum {
    MVR_IN  = 0,
    MVR_OUT = 1,
    MVR_TARGET = 2,
    MVR_COMPR  = 3,
};

typedef struct {
    // Features
    LV2_URID_Map*  map;
    LV2_Log_Logger logger;

    // Ports
    const LV2_Atom_Sequence* in_port;
    LV2_Atom_Sequence*       out_port;
    const float *target_velocity_port;
    const float *compression_perc_port;

    // URIs
    MVRURIs uris;
} MVR;

static void
connect_port(LV2_Handle instance,
             uint32_t   port,
             void*      data)
{
    MVR* self = (MVR*)instance;
    switch (port) {
    case MVR_IN:
        self->in_port = (const LV2_Atom_Sequence*)data;
        break;
    case MVR_OUT:
        self->out_port = (LV2_Atom_Sequence*)data;
        break;
    case MVR_TARGET:
        self->target_velocity_port = (float *)data;
        break;
    case MVR_COMPR:
        self->compression_perc_port = (float *)data;
        break;
    default:
        break;
    }
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               path,
            const LV2_Feature* const* features)
{
    // Allocate and initialise instance structure.
    MVR* self = (MVR*)calloc(1, sizeof(MVR));
    if (!self) {
        return NULL;
    }

    // Scan host features for URID map
    const char*  missing = lv2_features_query(
        features,
        LV2_LOG__log,  &self->logger.log, false,
        LV2_URID__map, &self->map,        true,
        NULL);
    lv2_log_logger_set_map(&self->logger, self->map);
    if (missing) {
        lv2_log_error(&self->logger, "Missing feature <%s>\n", missing);
        free(self);
        return NULL;
    }

    map_mvr_uris(self->map, &self->uris);

    return (LV2_Handle)self;
}

static void
cleanup(LV2_Handle instance)
{
    free(instance);
}

static void
run(LV2_Handle instance,
    uint32_t   sample_count)
{
    MVR*     self = (MVR*)instance;
    MVRURIs* uris = &self->uris;

    // Struct for a 3 byte MIDI event, used for writing notes
    typedef struct {
        LV2_Atom_Event event;
        uint8_t        msg[3];
    } MIDINoteEvent;

    // Initially self->out_port contains a Chunk with size set to capacity

    // Get the capacity
    const uint32_t out_capacity = self->out_port->atom.size;

    // Write an empty Sequence header to the output
    lv2_atom_sequence_clear(self->out_port);
    self->out_port->atom.type = self->in_port->atom.type;

    // Read incoming events
    LV2_ATOM_SEQUENCE_FOREACH(self->in_port, ev) {
        if (ev->body.type == uris->midi_Event) {
            const uint8_t* const msg = (const uint8_t*)(ev + 1);
            switch (lv2_midi_message_type(msg)) {
            case LV2_MIDI_MSG_NOTE_ON:
            case LV2_MIDI_MSG_NOTE_OFF:
                if (msg[1] <= 127) {
                    MIDINoteEvent note;

                    note.event= *ev;

                    note.msg[0] = msg[0];   // Same status
                    note.msg[1] = msg[1];   // Same note
                    if((msg[2] == 0) || (*self->compression_perc_port == 0)) {
                        note.msg[2] = msg[2];    // Preserve velocity
                    } else {
                        float diff = msg[2] - *self->target_velocity_port;
                        float velocity = *self->target_velocity_port + (((100-*self->compression_perc_port)*diff)/100);
                        //Clip to midi range
                        if(velocity < 1.0f) {
                            velocity = 1.0f;
                        } else if (velocity > 127.0f) {
                            velocity = 127.0f;
                        }
                        note.msg[2] = (uint8_t)velocity;
                    }
                    // Write note event
                    lv2_atom_sequence_append_event(
                        self->out_port, out_capacity, &note.event);
                }
                break;
            default:
                // Forward all other MIDI events directly
                lv2_atom_sequence_append_event(
                    self->out_port, out_capacity, ev);
                break;
            }
        }
    }
}

static const void*
extension_data(const char* uri)
{
    return NULL;
}

static const LV2_Descriptor descriptor = {
    GAE_MVR_URI,
    instantiate,
    connect_port,
    NULL,  // activate,
    run,
    NULL,  // deactivate,
    cleanup,
    extension_data
};

LV2_SYMBOL_EXPORT
const LV2_Descriptor* lv2_descriptor(uint32_t index)
{
    switch (index) {
    case 0:
        return &descriptor;
    default:
        return NULL;
    }
}
