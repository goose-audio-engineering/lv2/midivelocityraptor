## MIDIVelocityRaptor

This is a really simple MIDI domain compressor-type plugin. You can set a
preferred velocity level and a percentage of "compression" to be applied.
100% compression means all notes are on the exact velocity value dialed in
0% means pass through.
